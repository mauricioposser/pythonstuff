# Conteúdos Python

Repositório com conteúdos relacionados à linguagem Python.

## Folder Python

  * [EPMProcessorMinitreinamento](python/EPMProcessorMinitreinamento): Exemplo de uso da Web API Python (beta version) do EPM Processor.
  * [Howtos](python/Howtos): Exemplos diversos de uso da linguagem Python nas mais diversas etapas de um processo de análise de dados.
  * [Plugins](python/Plugins): Exemplo de plugins em lingaugem Python para a ferramenta EPM Studio Dataset Analyis.

## Folder pythonparaengenehiros

bkp do conteúdo do livro Python para Engenheiros que está em desenvolvimento e disponível no GitBook:
[Python para Engenheiros](https://mauricioposser.gitbook.io/pythonparaengenheiros/)

